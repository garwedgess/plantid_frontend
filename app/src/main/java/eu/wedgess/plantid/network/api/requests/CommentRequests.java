/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.network.api.requests;

import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.network.api.responses.CommentResponse;
import eu.wedgess.plantid.network.api.responses.CommentsResponse;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Retrofit class ot call the servers API endpoints for comment items. {@link Comment}
 * Used by {@link eu.wedgess.plantid.data.source.remote.CommentsRemoteDataSource}
 * <p>
 * Created by gar on 02/01/18.
 */

public interface CommentRequests {

    @Headers("Content-Type: application/json")
    @POST("/threads/{tid}/comments")
    Completable createComment(@Path("tid") String threadId, @Body Comment comment);

    @GET("/threads/{tid}/comments")
    Observable<CommentsResponse> getCommentsByThreadId(@Path("tid") String tid);

    @GET("/threads/{tid}/comments/{cid}")
    Single<CommentResponse> getCommentById(@Path("tid") String tid, @Path("cid") String cid);

    @Headers("Content-Type: application/json")
    @PUT("/threads/{tid}/comments/{cid}")
    Completable updateComment(@Path("tid") String tid, @Path("cid") String cid, @Body Comment comment);

    @DELETE("/threads/{tid}/comments/{cid}")
    Completable deleteComment(@Path("tid") String tid, @Path("cid") String cid);

}
