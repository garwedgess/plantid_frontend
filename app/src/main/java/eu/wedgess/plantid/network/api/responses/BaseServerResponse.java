/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.network.api.responses;

import com.google.gson.annotations.Expose;

/**
 * Base response class, all responses will extend from thsi class adding their own data types
 * all responses take the format:
 * <p>
 * error - boolean
 * message - String
 * <p>
 * Created by gar on 28/10/17.
 */

public abstract class BaseServerResponse {

    static final String KEY_DATA = "data";

    @Expose
    private final boolean error;
    @Expose
    private final String message;

    public BaseServerResponse(boolean error, String message) {
        this.error = error;
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
