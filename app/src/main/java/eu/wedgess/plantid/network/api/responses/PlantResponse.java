/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.network.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import eu.wedgess.plantid.data.Plant;

/**
 * Class for getting a single plant response data from server. {@link Plant}
 * All responses take the format:
 * error - boolean
 * message - String
 * data - return data
 * <p>
 * {@link SerializedName} is used by GSON to parse the data
 * <p>
 * Created by gar on 28/10/17.
 */

public class PlantResponse extends BaseServerResponse {

    @Expose
    @SerializedName(KEY_DATA)
    private final Plant plant;

    public PlantResponse(boolean error, String message, Plant plant) {
        super(error, message);
        this.plant = plant;
    }

    public Plant getPlant() {
        return plant;
    }
}
