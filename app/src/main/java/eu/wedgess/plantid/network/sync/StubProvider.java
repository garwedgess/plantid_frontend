package eu.wedgess.plantid.network.sync;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

/**
 * Define an implementation of ContentProvider that stubs out
 * all methods. This app does not use a ContentProvider however a mock is required
 * for {@link SyncAdapter} to work.
 * <p>
 * Created by gar: 10/02/2018
 **/
public class StubProvider extends ContentProvider {

    /**
     * The authority for the sync adapter's content provider
     * The authority must be the same as that provided in /res/xml/syncadapter.xml
     */
    public static final String AUTHORITY = StubProvider.class.getCanonicalName();

    /*
     * Always return true, indicating that the
     * provider loaded correctly.
     */
    @Override
    public boolean onCreate() {
        return true;
    }

    /*
     * Return no type for MIME type
     */
    @Override
    public String getType(Uri uri) {
        return null;
    }

    /*
     * query() always returns no results
     *
     */
    @Override
    public Cursor query(
            Uri uri,
            String[] projection,
            String selection,
            String[] selectionArgs,
            String sortOrder) {
        return null;
    }

    /*
     * insert() always returns null (no URI)
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    /*
     * delete() always returns "no rows affected" (0)
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    /*
     * update() always returns "no rows affected" (0)
     */
    public int update(
            Uri uri,
            ContentValues values,
            String selection,
            String[] selectionArgs) {
        return 0;
    }
}
