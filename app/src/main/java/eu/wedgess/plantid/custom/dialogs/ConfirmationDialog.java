/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.custom.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import eu.wedgess.plantid.R;

/**
 * Wrap the AlertDialog inside a {@link DialogFragment} to retain dialog on orientation change.
 * Activities who create this dialog must implement the below {@link ConfirmationDialogCompleteListener}.
 * If calling from an activity be sure to call {@link #setListener(ConfirmationDialogCompleteListener)}
 * before showing the dialog.
 * <p>
 * Created by gar on 18/02/18.
 */

public class ConfirmationDialog extends DialogFragment {

    // interface to listen to confirm button click
    public interface ConfirmationDialogCompleteListener {
        void onConfirmationDialogConfirmClicked();
    }

    private static final String ARGS_KEY_TITLE = "ARGS_KEY_TITLE";
    private static final String ARGS_KEY_MESSAGE = "ARGS_KEY_MESSAGE";

    private String mTitle;
    private String mMessage;

    public void setListener(ConfirmationDialogCompleteListener mListener) {
        this.mListener = mListener;
    }

    private ConfirmationDialogCompleteListener mListener;

    public static ConfirmationDialog newInstance(String title, String message) {
        Bundle args = new Bundle();
        args.putString(ARGS_KEY_TITLE, title);
        args.putString(ARGS_KEY_MESSAGE, message);

        ConfirmationDialog fragment = new ConfirmationDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ConfirmationDialogCompleteListener) {
            mListener = (ConfirmationDialogCompleteListener) activity;
        } else if (mListener == null) {
            throw new RuntimeException("Activity must implement ConfirmationDialogCompleteListener!");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTitle = getArguments().getString(ARGS_KEY_TITLE);
        mMessage = getArguments().getString(ARGS_KEY_MESSAGE);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(mTitle)
                .setMessage(mMessage)
                .setPositiveButton(R.string.btn_confirm, (dialogInterface, i) -> mListener.onConfirmationDialogConfirmClicked())
                .setNegativeButton(R.string.btn_cancel, (dialogInterface, i) -> dialogInterface.cancel())
                .create();
    }
}
