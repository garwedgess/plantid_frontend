package eu.wedgess.plantid.features.plants;

import android.support.annotation.NonNull;
import android.util.Log;

import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.data.source.repositories.PlantsRepository;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Presenter for {@link PlantsFragment}
 * <p>
 * Created by gar on 09/11/17.
 */

public class PlantsPresenter implements PlantsContract.Presenter {

    private static final String TAG = PlantsPresenter.class.getSimpleName();

    @NonNull
    private final PlantsRepository mPlantsRepository;
    @NonNull
    private final PlantsContract.View mPlantsView;
    @NonNull
    private final CompositeDisposable mCompositeDisposable;
    @NonNull
    private PlantsFilterType mCurrentFiltering = PlantsFilterType.ALL_PLANTS;
    private String mSearchFilterQuery = null;

    PlantsPresenter(PlantsRepository plantsRepository, PlantsContract.View mPlantsView) {
        this.mPlantsRepository = checkNotNull(plantsRepository);
        this.mPlantsView = checkNotNull(mPlantsView);
        this.mPlantsView.setPresenter(this);

        mCompositeDisposable = new CompositeDisposable();
    }

    /**
     * Get the plants, if search query is set then filter plants based on the query by their common or botanical name.
     * If filtering by plant type, apply that.
     * Sort the list alphabetically by common name
     *
     * @param showLoadingUI Pass in true to display a loading icon in the UI
     */
    @Override
    public void loadPlants(boolean showLoadingUI) {
        if (showLoadingUI) {
            mPlantsView.setLoadingIndicator(true);
        }

        Disposable disposable = mPlantsRepository
                .getPlants()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(Observable::fromIterable)
                .filter(plant -> mSearchFilterQuery == null
                        || (plant.getBotanicalName().toLowerCase().contains(mSearchFilterQuery)
                        || plant.getCommonName().toLowerCase().contains(mSearchFilterQuery)))
                .filter(plant -> mCurrentFiltering.getStringType().equalsIgnoreCase(PlantsFilterType.ALL_PLANTS.getStringType())
                        || plant.getType().equalsIgnoreCase(mCurrentFiltering.getStringType()))
                .toSortedList((plant, other) -> plant.getCommonName().compareToIgnoreCase(other.getCommonName()))
                .subscribe(
                        // onNext
                        plants -> {
                            if (plants == null || plants.isEmpty()) {
                                mPlantsView.showNoPlantData();
                            } else {
                                if (showLoadingUI) {
                                    mPlantsView.setLoadingIndicator(false);
                                }
                                mPlantsView.setPlantsData(plants);
                                showFilterLabel();
                            }
                        },
                        // onError
                        throwable -> {
                            Log.i(TAG, "error loading plants");
                            mPlantsView.showLoadingPlantsError();
                        });

        mCompositeDisposable.add(disposable);
    }

    @Override
    public void openPlantDetailsActivity(@NonNull Plant requestedPlant) {
        checkNotNull(requestedPlant, "requestedPlant cannot be null!");
        mPlantsView.showPlantDetailsUi(requestedPlant.getId());
    }

    /**
     * Sets the current task filtering type.
     *
     * @param requestType Can be {@link PlantsFilterType#ALL_PLANTS},
     *                    {@link PlantsFilterType#PERENNIAL_ONLY},
     *                    {@link PlantsFilterType#SHRUBS_ONLY} or
     *                    {@link PlantsFilterType#TREES_ONLY}
     */
    @Override
    public void setFiltering(@NonNull PlantsFilterType requestType) {
        mCurrentFiltering = requestType;
        showFilterLabel();
    }

    @Override
    public PlantsFilterType getFiltering() {
        return mCurrentFiltering;
    }

    @Override
    public void setSearchQuery(String query) {
        mSearchFilterQuery = query != null ? query.toLowerCase() : query;
    }

    @Override
    public String getSearchFilterQuery() {
        return this.mSearchFilterQuery;
    }

    // depending on filter type chosen show the title in toolbar
    private void showFilterLabel() {
        switch (mCurrentFiltering) {
            case SHRUBS_ONLY:
                mPlantsView.showShrubsFilterLabel();
                break;
            case TREES_ONLY:
                mPlantsView.showTreesFilterLabel();
                break;
            case PERENNIAL_ONLY:
                mPlantsView.showPerenialsFilterLabel();
                break;
            case ALPINE_ONLY:
                mPlantsView.showAlpineFilterLabel();
                break;
            case ANNUAL_ONLY:
                mPlantsView.showAnnualFilterLabel();
                break;
            case CONSERVATORY_ONLY:
                mPlantsView.showConservatoryFilterLabel();
                break;
            case CLIMBER_ONLY:
                mPlantsView.showClimbersFilterLabel();
                break;
            case BULBS_ONLY:
                mPlantsView.showBulbsFilterLabel();
                break;
            case ALL_PLANTS:
            default:
                mPlantsView.showAllFilterLabel();
                break;
        }
    }

    @Override
    public void subscribe() {
        loadPlants(true);
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }
}
