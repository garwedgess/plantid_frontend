/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.register;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.io.File;
import java.net.URISyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.source.local.UsersLocalDataSource;
import eu.wedgess.plantid.data.source.remote.UsersRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import eu.wedgess.plantid.features.mainnavigation.MainNavigationActivity;
import eu.wedgess.plantid.helpers.GlideImageHelper;
import eu.wedgess.plantid.utils.FileUtils;
import eu.wedgess.plantid.utils.NetworkUtils;

/**
 * Register activity where users can register an account.
 */
public class RegisterActivity extends BaseActivity implements RegisterContract.View, View.OnClickListener {

    private static final String TAG = RegisterActivity.class.getSimpleName();
    private static final int REQUEST_CODE_PROFILE_IMG = 312;

    //region View binding
    @BindView(R.id.toolbar_register)
    Toolbar toolbar;
    @BindView(R.id.tv_reg_txt)
    TextView mRegisteringText;
    @BindView(R.id.til_reg_username)
    TextInputLayout mUsernameTIL;
    @BindView(R.id.til_reg_password)
    TextInputLayout mPasswordTIL;
    @BindView(R.id.til_reg_confirm_password)
    TextInputLayout mPasswordConfirmationTIL;
    @BindView(R.id.btn_register)
    Button mRegisterBTN;
    @BindView(R.id.pb_registering)
    ProgressBar mRegisteringPB;
    @BindView(R.id.iv_user_profile_img)
    ImageView mUserProfileImgIV;
    @BindView(R.id.fab_select_profile_img)
    FloatingActionButton mSelectProfileImgFAB;
    //endregion

    private String mProfileImgPath;
    private RegisterContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        // disable toolbar title
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRegisterBTN.setOnClickListener(this);
        mSelectProfileImgFAB.setOnClickListener(this);

        // add text change listener so that username cannot contain spaces
        mUsernameTIL.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().contains(" ")) {
                    mUsernameTIL.setErrorEnabled(true);
                    mUsernameTIL.setError(getString(R.string.error_username_contains_spaces));
                } else if (!TextUtils.isEmpty(editable.toString()) && mUsernameTIL.isErrorEnabled()) {
                    mUsernameTIL.setErrorEnabled(false);
                }
            }
        });

        new RegisterPresenter(
                UserRepository.getInstance(
                        new UsersLocalDataSource(AppDatabase.getInstance(RegisterActivity.this).userDao(),
                                PreferenceManager.getDefaultSharedPreferences(RegisterActivity.this)),
                        new UsersRemoteDataSource()),
                this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(@NonNull RegisterContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setRegisteringIndicator(boolean active) {
        if (active) {
            setRegistrationErrorText(false);
            mRegisteringText.setVisibility(View.VISIBLE);
            mRegisteringPB.setVisibility(View.VISIBLE);
            if (mPasswordTIL.isErrorEnabled()) {
                mPasswordTIL.setErrorEnabled(false);
            }
            if (mUsernameTIL.isErrorEnabled()) {
                mUsernameTIL.setErrorEnabled(false);
            }
        } else {
            mRegisteringText.setVisibility(View.GONE);
            mRegisteringPB.setVisibility(View.GONE);

        }
    }

    @Override
    public void setRegistrationErrorText(boolean showError) {
        if (showError) {
            mRegisteringText.setText(R.string.register_account_creation_error);
            mRegisteringText.setTextColor(ContextCompat.getColor(this, R.color.colorError));
            mRegisteringText.setVisibility(View.VISIBLE);
        } else {
            mRegisteringText.setText(R.string.register_loading_text);
            mRegisteringText.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    @Override
    public void setUsernameEmptyError() {
        mUsernameTIL.setErrorEnabled(true);
        mUsernameTIL.setError(getString(R.string.error_username_empty));

    }

    @Override
    public void setPasswordEmptyError() {
        mPasswordTIL.setErrorEnabled(true);
        mPasswordTIL.setError(getString(R.string.error_password_empty));
    }

    @Override
    public void setPasswordConfirmationEmptyError() {
        mPasswordConfirmationTIL.setErrorEnabled(true);
        mPasswordConfirmationTIL.setError(getString(R.string.error_password_confirmation_empty));
    }

    @Override
    public void setPasswordConfirmationNoMatchError() {
        mPasswordConfirmationTIL.setErrorEnabled(true);
        mPasswordConfirmationTIL.setError(getString(R.string.error_password_confirmation_no_match));
    }

    @Override
    public void setSuccessfulRegistration() {
        // return the LoginActivity which will finish and return to UserDetailsFragment
        setResult(RESULT_OK);
        finish();
    }

    private void openGalleryImagePicker() {
        // open media picker chooser to select image
        Intent intent = new Intent();
        // the type which we will be picking to upload is image of any format
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.title_select_image)), REQUEST_CODE_PROFILE_IMG);
        overridePendingTransition(R.anim.activity_slide_up, R.anim.activity_stay);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestStoragePermissions() {
        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MainNavigationActivity.REQUEST_CODE_PERMISSION_STORAGE);
    }

    private boolean hasStoragePermissions() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void showStoragePermissionSnackbar() {
        Snackbar.make(findViewById(android.R.id.content), R.string.permission_grant_storage,
                Snackbar.LENGTH_INDEFINITE).setAction(R.string.btn_enable,
                v -> requestStoragePermissions()).show();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_register) {
            // only attempt to register if we have an internet connection
            if (NetworkUtils.isNetworkAvailable((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
                String usernameField = mUsernameTIL.getEditText().getText().toString();
                String passwordField = mPasswordTIL.getEditText().getText().toString();
                String passwordConfirmation = mPasswordConfirmationTIL.getEditText().getText().toString();

                // validate input fields, and if valid then submit
                if (mPresenter.validateRegistrationFields(usernameField, passwordField, passwordConfirmation)) {
                    mPresenter.performRegistration(
                            mUsernameTIL.getEditText().getText().toString(),
                            mPasswordTIL.getEditText().getText().toString(),
                            mPasswordConfirmationTIL.getEditText().getText().toString(),
                            Strings.isNullOrEmpty(mProfileImgPath) ? null : new File(mProfileImgPath));
                }
            } else {
                mRegisteringText.setVisibility(View.VISIBLE);
                mRegisteringText.setText(R.string.login_error_no_internet);
            }
        } else if (view.getId() == R.id.fab_select_profile_img) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (hasStoragePermissions()) {
                    openGalleryImagePicker();
                } else {
                    requestStoragePermissions();
                }
            } else {
                openGalleryImagePicker();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PROFILE_IMG && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the image path from Gallery
                String imgPath = FileUtils.getRealPathFromDocumentUri(getApplicationContext(), filePath);
                GlideImageHelper.glideLoadCircleImage(mUserProfileImgIV, imgPath);
                mProfileImgPath = imgPath;
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MainNavigationActivity.REQUEST_CODE_PERMISSION_STORAGE:
                if (grantResults.length > 0) {
                    boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (readExternalFile) {
                        // open media picker
                        openGalleryImagePicker();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            showStoragePermissionSnackbar();
                        }
                    }
                }
        }
    }
}
