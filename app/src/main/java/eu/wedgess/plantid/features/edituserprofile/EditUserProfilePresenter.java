/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.edituserprofile;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.base.Optional;
import com.google.common.base.Strings;

import java.io.File;

import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import eu.wedgess.plantid.helpers.CurrentUserInstance;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The presenter for the view {@link EditUserProfileActivity}
 * <p>
 * Created by gar on 09/11/17.
 */

public class EditUserProfilePresenter implements EditUserProfileContract.Presenter {

    private static final String TAG = EditUserProfilePresenter.class.getSimpleName();

    // get either all data or user profile image
    enum UserDataType {
        ALL_DATA, USER_IMG
    }

    @NonNull
    private final CompositeDisposable mCompositeDisposable;
    private final UserRepository mUserRepository;
    private final EditUserProfileContract.View mEditProfileView;

    EditUserProfilePresenter(@NonNull UserRepository mUserRepository,
                             @NonNull EditUserProfileContract.View mEditProfileView) {
        this.mUserRepository = checkNotNull(mUserRepository);
        this.mEditProfileView = checkNotNull(mEditProfileView);
        this.mEditProfileView.setPresenter(this);

        mCompositeDisposable = new CompositeDisposable();
    }

    private void getUserDetails(UserDataType userDataType) {
        // fetch current user from prefs, user is an optional so make sure it is present
        mCompositeDisposable.add(mUserRepository.getCurrentUser()
                .subscribeOn(Schedulers.io())
                .filter(Optional::isPresent)
                .map(Optional::get)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                            switch (userDataType) {
                                case ALL_DATA:
                                    showUserDetails(user);
                                    break;
                                case USER_IMG:
                                    mEditProfileView.setProfileImage(user.getImgUrl());
                                    break;
                                default:
                                    showUserDetails(user);
                                    break;
                            }
                        },
                        throwable -> {
                            // handle error event
                            Log.e(TAG, "Error user observable");
//                            throw Exceptions.propagate(throwable);
                        }, () -> {
                            // handle on complete event
                            Log.i(TAG, "Get user observable completed");
                        }));

    }

    @Override
    public void getUserProfileImage() {
        getUserDetails(UserDataType.USER_IMG);
    }

    @Override
    public void updateUser(@NonNull String username, @Nullable String imgPath) {
        // if username is empty show error message else if it contains spaces show error, otherwise update user
        if (Strings.isNullOrEmpty(username)) {
            mEditProfileView.setEmptyUsernameError();
        } else if (username.contains(" ")) {
            mEditProfileView.setUsernameContainsSpacesError();
        } else {
            User user = CurrentUserInstance.getInstance().getCurrentUser();
            // keep old username in case of failed update
            final String oldUsername = user.getUserName();
            user.setUsername(username);
            checkNotNull(user, "User is null");
//            checkNotNull()
            File imgFile = null;
            if (imgPath != null) {
                imgFile = new File(imgPath);
            }
            mCompositeDisposable.add(
                    mUserRepository
                            .updateUserProfile(user, imgFile)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(user1 ->
                                            mCompositeDisposable.add(mUserRepository
                                                    .insertCurrentUserToPreferences(user1)
                                                    .subscribeOn(Schedulers.io())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(() ->
                                                            mEditProfileView.userUpdatedSuccessfully())),
                                    throwable -> {
                                        // if update fails, reset username to old username and show failed update toast
                                        user.setUsername(oldUsername);
                                        mEditProfileView.failedToUpdateUser();
                                    }));
        }
    }

    // set user details (profile image and username)
    private void showUserDetails(User user) {
        if (!Strings.isNullOrEmpty(user.getImgUrl())) {
            mEditProfileView.setProfileImage(user.getImgUrl());
        }
        if (!Strings.isNullOrEmpty(user.getUserName())) {
            mEditProfileView.setProfileUsername(user.getUserName());
        }
    }

    @Override
    public void subscribe() {
        getUserDetails(UserDataType.ALL_DATA);
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }
}
