/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.wedgess.plantid.BaseActivity;
import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.source.local.UsersLocalDataSource;
import eu.wedgess.plantid.data.source.remote.UsersRemoteDataSource;
import eu.wedgess.plantid.data.source.repositories.UserRepository;
import eu.wedgess.plantid.features.register.RegisterActivity;
import eu.wedgess.plantid.utils.NetworkUtils;

/**
 * A login activity that is called from {@link eu.wedgess.plantid.features.userdetails.UserDetailsFragment}
 * If the user hasn't already got an account then then they can register from this activity which calls
 * {@link RegisterActivity}.
 */
public class LoginActivity extends BaseActivity implements LoginContract.View, View.OnClickListener {

    public static final int RESULT_CODE_REGISTER = 128;

    //region View binding
    @BindView(R.id.toolbar_login)
    Toolbar toolbar;
    @BindView(R.id.tv_register_value)
    TextView mRegisterTV;
    @BindView(R.id.tv_logging_in)
    TextView mLogingInTV;
    @BindView(R.id.til_username)
    TextInputLayout mUsernameTIL;
    @BindView(R.id.til_password)
    TextInputLayout mPasswordTIL;
    @BindView(R.id.btn_login)
    Button mLoginBTN;
    @BindView(R.id.pb_logging_in)
    ProgressBar mLoginPB;
    @BindView(R.id.rlayout_register)
    RelativeLayout mRegisterLayout;
    //endregion

    private LoginContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // add underline to register text
        mRegisterTV.setPaintFlags(mRegisterTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mLoginBTN.setOnClickListener(this);
        mRegisterLayout.setOnClickListener(this);

        mUsernameTIL.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // check if username contains spaces
                if (editable.toString().contains(" ")) {
                    mUsernameTIL.setErrorEnabled(true);
                    mUsernameTIL.setError(getString(R.string.error_username_contains_spaces));
                } else if (!TextUtils.isEmpty(editable.toString()) && mUsernameTIL.isErrorEnabled()) {
                    mUsernameTIL.setErrorEnabled(false);
                }
            }
        });

        // setup login presenter
        new LoginPresenter(
                UserRepository.getInstance(
                        new UsersLocalDataSource(AppDatabase.getInstance(LoginActivity.this).userDao(),
                                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this)),
                        new UsersRemoteDataSource()),
                this);
        setPresenter(mPresenter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(@NonNull LoginContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setLoggingInIndicator(boolean active) {
        // show logging in progress ui
        if (active) {
            setLoginErrorText(false);
            mLogingInTV.setVisibility(View.VISIBLE);
            mLoginPB.setVisibility(View.VISIBLE);
            if (mPasswordTIL.isErrorEnabled()) {
                mPasswordTIL.setErrorEnabled(false);
            }
            if (mUsernameTIL.isErrorEnabled()) {
                mUsernameTIL.setErrorEnabled(false);
            }
        } else {
            // hide progress ui
            mLogingInTV.setVisibility(View.GONE);
            mLoginPB.setVisibility(View.GONE);

        }
    }

    @Override
    public void setLoginErrorText(boolean showError) {
        if (showError) {
            mLogingInTV.setText(R.string.error_login_invalid_credentials);
            mLogingInTV.setTextColor(ContextCompat.getColor(this, R.color.colorError));
            mLogingInTV.setVisibility(View.VISIBLE);
        } else {
            mLogingInTV.setText(R.string.login_msg_logging_in);
            mLogingInTV.setTextColor(ContextCompat.getColor(this, R.color.white));
        }
    }

    @Override
    public void setUsernameEmptyError() {
        mUsernameTIL.setErrorEnabled(true);
        mUsernameTIL.setError(getString(R.string.error_username_empty));

    }

    @Override
    public void setPasswordEmptyError() {
        mPasswordTIL.setErrorEnabled(true);
        mPasswordTIL.setError(getString(R.string.error_password_empty));
    }

    @Override
    public void setSuccessfulLogin() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onClick(View view) {
        // ensure network connection is available before attempting login
        if (view.getId() == R.id.btn_login) {
            if (NetworkUtils.isNetworkAvailable(((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)))) {
                String usernameField = mUsernameTIL.getEditText().getText().toString();
                String passwordField = mPasswordTIL.getEditText().getText().toString();
                // validate login info
                if (mPresenter.validateLoginInfo(usernameField, passwordField)) {
                    mPresenter.performLogin(usernameField, passwordField);
                }
            } else {
                mLogingInTV.setVisibility(View.VISIBLE);
                mLogingInTV.setText(R.string.login_error_no_internet);
            }
        } else if (view.getId() == R.id.rlayout_register) {
            // open register activity
            startActivityForResult(new Intent(LoginActivity.this, RegisterActivity.class), RESULT_CODE_REGISTER);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_CODE_REGISTER) {
            if (resultCode == RESULT_OK) {
                // if registered ok exit this activity to return to UserDetailsFragment
                setResult(RESULT_OK);
                finish();
            }
        }
    }
}
