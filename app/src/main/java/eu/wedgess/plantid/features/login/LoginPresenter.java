/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.login;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.base.Strings;

import eu.wedgess.plantid.data.source.repositories.UserRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Presenter for {@link LoginActivity}
 * <p>
 * Created by gar on 09/11/17.
 */
public class LoginPresenter implements LoginContract.Presenter {

    private static final String TAG = LoginPresenter.class.getSimpleName();

    private final UserRepository mUserRepository;
    private final LoginContract.View mLoginView;
    private final CompositeDisposable mCompositeDisposable;

    LoginPresenter(UserRepository userRepository, LoginContract.View loginView) {
        this.mUserRepository = checkNotNull(userRepository);
        this.mLoginView = checkNotNull(loginView);
        mCompositeDisposable = new CompositeDisposable();
        mLoginView.setPresenter(this);
    }

    @Override
    public void subscribe() {
        // do nothing on subscribe
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public void performLogin(@NonNull String username, @NonNull String password) {
        mLoginView.setLoggingInIndicator(true);

        // log user in and on success insert the current user to shared preferences
        mCompositeDisposable.add(mUserRepository.logUserIn(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user ->
                                mCompositeDisposable.add(
                                        mUserRepository
                                                .insertCurrentUserToPreferences(user)
                                                .subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .doOnComplete(() -> {
                                                    mLoginView.setLoggingInIndicator(false);
                                                    mLoginView.setSuccessfulLogin();
                                                })
                                                .subscribe(() -> Log.i(TAG, "Current user inserted into preferences"),
                                                        throwable -> Log.e(TAG, "error inserting current user to preferences"))),
                        throwable -> {
                            Log.e(TAG, "error could not log in");
                            mLoginView.setLoggingInIndicator(false);
                            mLoginView.setLoginErrorText(true);
//                    throw Exceptions.propagate(throwable);
                        }));
    }

    @Override
    public boolean validateLoginInfo(@NonNull String username, @NonNull String password) {
        if (Strings.isNullOrEmpty(username)) {
            mLoginView.setUsernameEmptyError();
            mLoginView.setLoggingInIndicator(false);
            return false;
        }
        if (Strings.isNullOrEmpty(password)) {
            mLoginView.setPasswordEmptyError();
            mLoginView.setLoggingInIndicator(false);
            return false;
        }
        return true;
    }
}
