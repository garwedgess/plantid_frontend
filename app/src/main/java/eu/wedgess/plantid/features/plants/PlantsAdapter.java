/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.plants;

import android.annotation.SuppressLint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.wedgess.plantid.R;
import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.helpers.GlideImageHelper;

/**
 * An adapter for displaying a plant item in {@link PlantsFragment}
 * <p>
 * Created by gar on 31/10/17.
 */

public class PlantsAdapter extends RecyclerView.Adapter<PlantsAdapter.ViewHolder> {

    public static final String TAG = PlantsAdapter.class.getSimpleName();

    // click callback for activity
    public interface RecyclerClickListener {
        void onRecyclerItemClick(int position);
    }

    private List<Plant> mPlantsList;
    private RecyclerClickListener mRecyclerClickListener;

    PlantsAdapter(List<Plant> plants, RecyclerClickListener recyclerClickListener) {
        this.mPlantsList = plants;
        this.mRecyclerClickListener = recyclerClickListener;
    }

    // Create new views (invoked by the layout manager)
    @SuppressLint("InflateParams")
    @Override
    public PlantsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_plants, null), mRecyclerClickListener);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Plant plant = mPlantsList.get(position);
        viewHolder.setData(plant);
    }

    // Return the size of your mPlantsList (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mPlantsList.size();
    }

    public Plant getItemAtPos(int pos) {
        return this.mPlantsList.get(pos);
    }

    public ArrayList<Plant> getList() {
        return new ArrayList<>(this.mPlantsList);
    }

    // inner class to hold a reference to each item of RecyclerView
    static class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView rivPlantImage;
        final ImageView ivPlantType;
        final TextView tvPlantTitle;
        final TextView tvPlantSubtitle;
        final TextView tvPlantType;

        ViewHolder(View itemLayoutView, final RecyclerClickListener listener) {
            super(itemLayoutView);
            tvPlantTitle = itemLayoutView.findViewById(R.id.tv_plant_title);
            tvPlantSubtitle = itemLayoutView.findViewById(R.id.tv_plant_sub_title);
            tvPlantType = itemLayoutView.findViewById(R.id.tv_plant_type);
            ivPlantType = itemLayoutView.findViewById(R.id.iv_plant_type);
            rivPlantImage = itemLayoutView.findViewById(R.id.iv_plant_image);
            itemLayoutView.setOnClickListener(view -> listener.onRecyclerItemClick(getAdapterPosition()));
        }

        // set the data to views
        public void setData(Plant data) {
            tvPlantTitle.setText(data.getCommonName());
            tvPlantSubtitle.setText(data.getBotanicalName());
            // TODO: Can remove for production
            GlideImageHelper.glideLoadCircleImage(rivPlantImage, data.getImgUrl().replace("localhost", "10.0.2.2"));
            tvPlantType.setText(data.getType());

            ivPlantType.setImageDrawable(ContextCompat.getDrawable(ivPlantType.getContext(), getPlantTypeDrawableId(data.getType())));

        }

        private int getPlantTypeDrawableId(String plantType) {
            int iconId = -1;
            if (plantType.equalsIgnoreCase("cactus")) {
                iconId = R.drawable.ic_type_cactus;
            } else if (plantType.equalsIgnoreCase("tree")) {
                iconId = R.drawable.ic_type_tree;
            } else if (plantType.equalsIgnoreCase("climber")) {
                iconId = R.drawable.ic_type_creeper;
            } else {
                iconId = R.drawable.ic_type_plant;
            }
            return iconId;
        }
    }
}
