/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.mainnavigation;

/**
 * TODO: Need implementing or removing
 * Created by gar on 06/11/17.
 */

public class MainNavigationPresenter implements MainNavigationContract.Presenter {

    private static final String TAG = MainNavigationPresenter.class.getSimpleName();
    private MainNavigationContract.View mainView;

    MainNavigationPresenter(MainNavigationContract.View mainView) {
        this.mainView = mainView;
    }

    public MainNavigationContract.View getMainView() {
        return mainView;
    }
}
