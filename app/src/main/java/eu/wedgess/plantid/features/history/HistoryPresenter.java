package eu.wedgess.plantid.features.history;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.base.Optional;

import java.util.List;

import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.data.User;
import eu.wedgess.plantid.data.source.repositories.HistoryRepository;
import eu.wedgess.plantid.data.source.repositories.PlantsRepository;
import eu.wedgess.plantid.helpers.CurrentUserInstance;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Presenter for {@link HistoryActivity}
 * <p>
 * Created by gar on 09/11/17.
 */

public class HistoryPresenter implements HistoryContract.Presenter {

    private static final String TAG = HistoryPresenter.class.getSimpleName();

    private final HistoryRepository mHistoryRepository;
    private final PlantsRepository mPlantsRepository;
    private final HistoryContract.View mHistoryView;
    private final CompositeDisposable mCompositeDisposable;
    private int mHistoryType = HistoryActivity.TYPE_MATCHED;

    HistoryPresenter(HistoryRepository historyRepository, PlantsRepository plantsRepository, HistoryContract.View mHistoryView, int historyType) {
        this.mHistoryRepository = checkNotNull(historyRepository);
        this.mPlantsRepository = checkNotNull(plantsRepository);
        this.mHistoryView = checkNotNull(mHistoryView);
        this.mHistoryView.setPresenter(this);
        this.mHistoryType = historyType;
        this.mCompositeDisposable = new CompositeDisposable();
    }

    /**
     * Loads history into the view
     */
    private void loadHistory() {

        boolean showMatched = mHistoryType == HistoryActivity.TYPE_MATCHED;


        User currentUser = CurrentUserInstance.getInstance().getCurrentUser();
        if (currentUser != null) {
            mHistoryView.setLoadingIndicator(true);
            /*
             * Get user history and filter for matched or unmatched, if it is matched then
             * get the plant id of the matched plant.
             * Sort the list by date with most recent hostory item first.
             */
            mCompositeDisposable.add(mHistoryRepository.getHistory(currentUser.getUid())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .flatMap(Observable::fromIterable)
                    .filter(history -> showMatched == history.isMatch())
                    .flatMap(history -> {
                        if (showMatched) {
                            return mPlantsRepository.getPlantById(history.getPlantId())
                                    .toObservable()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .map(new Function<Optional<Plant>, History>() {
                                        @Override
                                        public History apply(Optional<Plant> plant) throws Exception {
                                            if (plant.isPresent()) {
                                                history.setPlantInfo(plant.get());
                                                Log.i(TAG, "Received plant: " + plant.get());
                                            }
                                            return history;
                                        }
                                    });
                        } else {
                            return Observable.just(history);
                        }
                    })
                    .toSortedList((history, t1) -> t1.getIdDate().compareTo(history.getIdDate()))
                    .subscribe(historyList -> {
                        mHistoryView.setLoadingIndicator(false);
                        if (historyList.isEmpty()) {
                            mHistoryView.showNoHistoryData();
                        } else {
                            mHistoryView.setHistoryData(historyList);
                        }
                    }, throwable -> {
                        mHistoryView.setLoadingIndicator(false);
                        Log.e(TAG, "Error fetching history");
                        mHistoryView.showLoadingHistoryError();
                    }));
        }
    }

    @Override
    public void subscribe() {
        loadHistory();
    }

    @Override
    public void unSubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public void deleteHistoryItems(@NonNull List<History> historyItems) {
        // delete multiple history items
        mCompositeDisposable.add(mHistoryRepository.deleteHistoryItems(historyItems)
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe(() ->
                                Log.i(TAG, "ALl history items deleted"),
                        throwable -> {
                            Log.e(TAG, "Error deleting historing items");
//                            throw Exceptions.propagate(throwable);
                        }));
    }
}
