/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.threaddetails;

import android.support.annotation.NonNull;

import java.util.List;

import eu.wedgess.plantid.BasePresenter;
import eu.wedgess.plantid.BaseView;
import eu.wedgess.plantid.data.Comment;

/**
 * Contract between {@link ThreadDetailActivity} and {@link ThreadDetailPresenter}
 * <p>
 * Created by gar on 02/12/17.
 */

class ThreadDetailContract {

    interface View extends BaseView<ThreadDetailPresenter> {

        void setCommentsUi(@NonNull List<Comment> commentList);

        void showMissingThread();

        void setThreadLoadingIndicator(boolean isLoading);

        void setCommentsLoadingIndicator(boolean isLoading);

        void setThreadTitle(@NonNull String title);

        void setThreadMessage(@NonNull String message);

        void setThreadAuthor(@NonNull String author);

        void setThreadAuthorProfileImage(@NonNull String imgUrl);

        void setThreadImage(@NonNull String imgUrl);

        void setThreadLocation(@NonNull String location);

        void setThreadDate(@NonNull String date);

        void showNoComments();

        void setIsThreadAuthor(@NonNull String authorId);

        void openEditThreadDetails(@NonNull String threadId);

        void threadDeleteComplete();

        void lockThread();

        boolean adapterIsNotCreated();

        void updateCommentsUi(@NonNull List<Comment> comments);

        void commentDeleted(int position);

        void openCreateCommentActivity();

        void showNotLoggedInError();
    }

    interface Presenter extends BasePresenter {

        void editThread();

        void deleteThread();

        void setCommentAsAnswer(@NonNull Comment commentItem);

        boolean isThreadSolved();

        void deleteComment(@NonNull Comment comment, int position);

        // user cannot post a comment if they are not logged in
        void canPostComment();

    }
}
