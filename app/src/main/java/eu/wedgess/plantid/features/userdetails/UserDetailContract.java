/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.features.userdetails;

import android.support.annotation.NonNull;

import java.util.List;

import eu.wedgess.plantid.BasePresenter;
import eu.wedgess.plantid.BaseView;
import eu.wedgess.plantid.data.Thread;

/**
 * Contract for {@link UserDetailsFragment} and {@link UserDetailsPresenter}
 * <p>
 * Created by gar on 09/11/17.
 */

class UserDetailContract {

    interface View extends BaseView<Presenter> {

        void showUserLocation();

        void hideUserLocation();

        void setUserProfileImage(@NonNull String imgUrl);

        void setUserProfileUsername(@NonNull String username);

        void setUserProfileDateCreated(@NonNull String dateCreated);

        void setSignedInButtonText();

        void setSignedOutButtonText();

        void setUserNotLoggedIn();

        void setMatchedHistoryCount(int count);

        void setUnmatchedHistoryCount(int count);

        void setMatchedHistoryCountNoAnim(int count);

        void setUnmatchedHistoryCountNoAnim(int count);

        void showNoHistoryItemsToast();

        boolean isActive();

        void setUsersThreads(@NonNull List<Thread> usersThreads);

        void showThreadDetailsUi(@NonNull String threadId);

        void showLoadingThreadsError();

        void hideUsersThreadsUi();

        boolean adapterIsNotCreated();

        void updateThreadsUi(@NonNull List<Thread> threads);

        void showUserActionsPanel();

        void hideUserActionsPanel();

    }

    interface Presenter extends BasePresenter {

        void getUserDetails();

        void updateHistoryData();

        boolean canOpenHistory(String historyCount);

        void signUserOut();

        void openThreadDetailsActivity(@NonNull Thread requestedThread);

        void getUsersThreads();

        void deleteUser();

    }
}
