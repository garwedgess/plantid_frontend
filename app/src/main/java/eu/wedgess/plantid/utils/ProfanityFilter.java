/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.utils;

import java.util.regex.Pattern;

/**
 * A profanity filter with words taken from: https://github.com/web-mech/badwords/blob/master/lib/lang.json
 * <p>
 * Created by gar on 28/02/18.
 **/

public class ProfanityFilter {

    public static final Pattern BAD_WORDS =
            Pattern.compile("(?<=\\b)((ahole)|(anus)|(ash0le)|(ash0les)|(asholes)|(ass)|(Ass Monkey)|" +
                    "(Assface)|(assh0le)|(assh0lez)|(asshole)|(assholes)|(assholz)|(asswipe)|(azzhole)|" +
                    "(bassterds)|(bastard)|(bastards)|(bastardz)|(basterds)|(basterdz)|(Biatch)|(bitch)|" +
                    "(bitches)|(Blow Job)|(boffing)|(butthole)|(buttwipe)|(c0ck)|(c0cks)|(c0k)|" +
                    "(Carpet Muncher)|(cawk)|(cawks)|(Clit)|(cnts)|(cntz)|(cock)|(cockhead)|(cock-head)|" +
                    "(cocks)|(CockSucker)|(cock-sucker)|(crap)|(cum)|(cunt)|(cunts)|(cuntz)|(dick)|(dickhead)|" +
                    "(dild0)|(dild0s)|(dildo)|(dildos)|(dilld0)|(dilld0s)|(dominatricks)|(dominatrics)|" +
                    "(dominatrix)|(dyke)|(enema)|(f u c k)|(f u c k e r)|(fag)|(fag1t)|(faget)|(fagg1t)|" +
                    "(faggit)|(faggot)|(fagit)|(fags)|(fagz)|(faig)|(faigs)|(fart)|(flipping the bird)|" +
                    "(fuck)|(fucker)|(fuckin)|(fucking)|(fucks)|(Fudge Packer)|(fuk)|(Fukah)|(Fuken)|" +
                    "(fuker)|(Fukin)|(Fukk)|(Fukkah)|(Fukken)|(Fukker)|(Fukkin)|(g00k)|(gay)|(gayboy)|" +
                    "(gaygirl)|(gays)|(gayz)|(God-damned)|(h00r)|(h0ar)|(h0re)|(hells)|(hoar)|(hoor)|" +
                    "(hoore)|(jackoff)|(jap)|(japs)|(jerk-off)|(jisim)|(jiss)|(jizm)|(jizz)|(knob)|" +
                    "(knobs)|(knobz)|(kunt)|(kunts)|(kuntz)|(Lesbian)|(Lezzian)|(Lipshits)|(Lipshitz)|" +
                    "(masochist)|(masokist)|(massterbait)|(masstrbait)|(masstrbate)|(masterbaiter)|" +
                    "(masterbate)|(masterbates)|(Motha Fucker)|(Motha Fuker)|(Motha Fukkah)|(Motha Fukker)|" +
                    "(Mother Fucker)|(Mother Fukah)|(Mother Fuker)|(Mother Fukkah)|(Mother Fukker)|" +
                    "(mother-fucker)|(Mutha Fucker)|(Mutha Fukah)|(Mutha Fuker)|(Mutha Fukkah)|" +
                    "(Mutha Fukker)|(n1gr)|(nastt)|(nigger;)|(nigur;)|(niiger;)|(niigr;)|(orafis)|" +
                    "(orgasim;)|(orgasm)|(orgasum)|(oriface)|(orifice)|(orifiss)|(packi)|(packie)|" +
                    "(packy)|(paki)|(pakie)|(paky)|(pecker)|(peeenus)|(peeenusss)|(peenus)|(peinus)|" +
                    "(pen1s)|(penas)|(penis)|(penis-breath)|(penus)|(penuus)|(Phuc)|(Phuck)|(Phuk)|" +
                    "(Phuker)|(Phukker)|(polac)|(polack)|(polak)|(Poonani)|(pr1c)|(pr1ck)|(pr1k)|" +
                    "(pusse)|(pussee)|(pussy)|(puuke)|(puuker)|(queer)|(queers)|(queerz)|(qweers)|" +
                    "(qweerz)|(qweir)|(recktum)|(rectum)|(retard)|(sadist)|(scank)|(schlong)|(screwing)|" +
                    "(semen)|(sex)|(sexy)|(Sh!t)|(sh1t)|(sh1ter)|(sh1ts)|(sh1tter)|(sh1tz)|(shits)|" +
                    "(shitter)|(Shitty)|(Shity)|(shitz)|(Shyt)|(Shyte)|(Shytty)|(Shyty)|(skanck)|(skank)|" +
                    "(skankee)|(skankey)|(skanks)|(Skanky)|(slag)|(sluts)|(Slutty)|(slutz)|" +
                    "(son-of-a-bitch)|(tit)|(turd)|(va1jina)|(vag1na)|(vagiina)|(vagina)|(vaj1na)|" +
                    "(vajina)|(vullva)|(vulva)|(w0p)|(wh00r)|(wh0re)|(xrated)|(xxx)|(b!+ch)" +
                    "|(blowjob)|(clit)|(arschloch)|(shit)|(b!tch)|(b17ch)|" +
                    "(b1tch)|(bi+ch)|(boiolas)|(buceta)|(chink)|(cipa)|(clits)" +
                    "|(dirsa)|(ejakulate)|(fatass)|(fcuk)|(fux0r)|" +
                    "(hore)|(jism)|(kawk)|(l3itch)|(l3i+ch)|(lesbian)|(masturbate)|(masterbat)|(masterbat3)|" +
                    "(motherfucker)|(s.o.b.)|(mofo)|(nazi)|(nigga)|(nutsack)|(phuck)|(pimpis)" +
                    "|(scrotum)|(shemale)|(shi+)|(sh!+)|(smut)|(teets)|(tits)|(boobs)|" +
                    "(b00bs)|(teez)|(testical)|(w00se)|(whoar)|" +
                    "(damn)|(amcik)|(andskota)|(arse)|(assrammer)|(ayir)|" +
                    "(bi7ch)|(bollock)|(breasts)|(butt-pirate)|(cabron)|(cazzo)|(chraa)|(chuj)|" +
                    "(Cock)|(d4mn)|(daygo)|(dego)|(dike)|(dupa)|(dziwka)|(ejackulate)|" +
                    "(Ekrem)|(Ekto)|(enculer)|(faen)|(fanculo)|(fanny)|(feces)|(feg)|(Felcher)|" +
                    "(ficken)|(fitt)|(Flikker)|(foreskin)|(Fotze)|(futkretzn)|(gook)|" +
                    "(guiena)|(h0r)|(h4x0r)|(hell)|(helvete)|(honkey)|(Huevon)|(hui)|(injun)|" +
                    "(kanker)|(kike)|(klootzak)|(kraut)|(knulle)|(kuk)|(kuksuger)|(Kurac)|(kurwa)|(kusi)|" +
                    "(kyrpa)|(lesbo)|(mamhoon)|(masturbat)|(merd)|(mibun)|(monkleigh)|(mouliewop)|(muie)|" +
                    "(mulkku)|(muschi)|(nazis)|(nepesaurio)|(orospu)|(paska)|(perse)|(picka)|" +
                    "(pierdol)|(pillu)|(pimmel)|(piss)|(pizda)|(poontsee)|(poop)|(porn)|(p0rn)|(pr0n)|" +
                    "(preteen)|(pula)|(pule)|(puta)|(puto)|(qahbeh)|(queef)|(rautenberg)|(schaffer)|(scheiss)|" +
                    "(schlampe)|(schmuck)|(screw)|(sharmuta)|(sharmute)|(shipal)|(shiz)|(skribz)|" +
                    "(skurwysyn)|(sphencter)|(spic)|(spierdalaj)|(splooge)|(suka)|(b00b)|(testicle)" +
                    "|(twat)|(vittu)|(wetback)|(wichser)|(wop)|(yed)|(zabourah))(?=\\b)", Pattern.CASE_INSENSITIVE);

    private ProfanityFilter() {
        // non instantiable
    }

    public static String filter(String text) {
        // split sentence into array of words
        String[] words = text.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            // replace bad words with *
            sb.append(BAD_WORDS.matcher(word).replaceAll(new String(new char[word.length()]).replace('\0', '*')));
            sb.append(" ");
        }
        // return trimmed string to remove trailing spaces
        return sb.toString().trim();
    }
}
