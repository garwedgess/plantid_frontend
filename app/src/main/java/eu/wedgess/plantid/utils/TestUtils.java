/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.utils;

import android.content.Context;
import android.text.TextUtils;

import java.io.IOException;
import java.io.InputStream;

/**
 * Testing utility to load json files used to simulate Network requests.
 * <p>
 * Created by gar on 28/10/17.
 */

public class TestUtils {

    public static String getPlantsResponseJSON(Context context) {
        return loadJSONFromAsset("plants.json", context);
    }

    private static String loadJSONFromAsset(String fileNameJSON, Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileNameJSON);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getUserResponseJSON(Context context) {
        return loadJSONFromAsset("user.json", context);
    }

    public static boolean nullOrEmpty(String value) {
        return value == null && TextUtils.isEmpty(value);
    }

}
