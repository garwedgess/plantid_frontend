/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.helpers;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import eu.wedgess.plantid.R;
import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.ColorFilterTransformation;

/**
 * An GlideImageHelper class for Glide library.
 * <p>
 * Created by gar on 31/10/17.
 */

public class GlideImageHelper {

    private static final String TAG = GlideImageHelper.class.getSimpleName();

    private static RequestOptions buildGlideRequestOptions() {
        return new RequestOptions()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
    }

    private static RequestOptions buildCircleTransformOptions() {
        return buildGlideRequestOptions().circleCrop();
    }

    private static RequestBuilder<Drawable> buildGlideErrorPlaceHolder(ImageView imageView) {
        RequestManager rm = Glide.with(imageView.getContext());
        RequestBuilder<Drawable> rb = rm.asDrawable();
        return rb.load(R.drawable.ic_error_outline);
    }

    private static RequestBuilder<Drawable> buildGlideCircleErrorPlaceHolder(ImageView imageView) {
        RequestManager rm = Glide.with(imageView.getContext());
        RequestBuilder<Drawable> rb = rm.asDrawable();
        return rb.apply(RequestOptions.circleCropTransform()).load(R.drawable.ic_error_outline);
    }

    @SuppressWarnings("unchecked")
    private static RequestOptions buildBlurredImageTransformOptions() {
        return buildGlideRequestOptions().bitmapTransform(new MultiTransformation(
                new BlurTransformation(60),
                new ColorFilterTransformation(R.color.blur_img_mask)));
    }

    private static RequestOptions buildFitCenterTransformOptions() {
        return buildGlideRequestOptions().fitCenter();
    }

    public static void glideLoadBlurredImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .apply(buildBlurredImageTransformOptions())
                .error(buildGlideErrorPlaceHolder(imageView))
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView);
    }

    public static void glideLoadCircleImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .apply(buildCircleTransformOptions())
                .error(buildGlideCircleErrorPlaceHolder(imageView))
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView);
    }

    public static void glideLoadImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .apply(buildFitCenterTransformOptions())
                .error(buildGlideErrorPlaceHolder(imageView))
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView);
    }
}
