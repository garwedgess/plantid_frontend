package eu.wedgess.plantid.data.source.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import eu.wedgess.plantid.data.Thread;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Database access object for accessing threads table {@link Thread}
 * <p>
 * Created by gar on 01/12/17.
 */

@Dao
public interface ThreadDao {

    @Query("SELECT * FROM threads")
    Single<List<Thread>> getAll();

    /**
     * Select a thread by id.
     *
     * @param tid the thread id.
     * @return the thread with pid.
     */
    @Query("SELECT * FROM threads WHERE id = :tid")
    Single<Thread> getThreadById(String tid);

    @Query("SELECT * FROM threads WHERE t_dirty = 1")
    Single<List<Thread>> getDirtyThreads();

    /**
     * Insert a thread in the database. If the thread already exists, replace it.
     *
     * @param thread the thread to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertThread(Thread thread);

    @Delete
    void deleteThread(Thread thread);

    /**
     * Update a thread.
     *
     * @param thread thread to be updated
     * @return the number of thread updated. This should always be 1.
     */
    @Update
    int updateThread(Thread thread);

    @Query("SELECT * FROM threads WHERE author_id = :authorId")
    Single<List<Thread>> getThreadsByAuthorId(String authorId);
}