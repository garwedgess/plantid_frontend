package eu.wedgess.plantid.data.source.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import eu.wedgess.plantid.data.History;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * History database access object for {@link History}.
 * <p>
 * Created by gar on 01/12/17.
 */
@Dao
public interface HistoryDao {

    @Query("SELECT * FROM history")
    Single<List<History>> getAll();

    @Query("SELECT * FROM history where h_id = :historyId")
    Single<History> findById(String historyId);

    @Query("SELECT * FROM history WHERE h_dirty = 1")
    Single<List<History>> getDirtyHistory();

    @Delete
    void deleteHistory(History history);

    @Delete
    void deleteAll(List<History> items);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(History history);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(History history);

}