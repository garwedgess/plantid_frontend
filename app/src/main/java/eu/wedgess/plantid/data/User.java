package eu.wedgess.plantid.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;
import java.util.UUID;

/**
 * User object:
 * <p>
 * {@link ColumnInfo} is for Room database column name mapping
 * {@link SerializedName} is for GSON mapping
 * <p>
 * Created by gar on 09/11/17.
 */

@Entity(tableName = "user")
public class User implements Parcelable {

    @NonNull
    @Expose
    @SerializedName("u_id")
    @PrimaryKey
    @ColumnInfo(name = "u_id")
    private String uid;

    @Nullable
    @Expose
    @SerializedName("u_uname")
    @ColumnInfo(name = "u_uname")
    private String userName;

    @Nullable
    @Expose
    @SerializedName("u_img_url")
    @ColumnInfo(name = "u_img_url")
    private String imgUrl;

    @Nullable
    @Expose
    @SerializedName("u_pwd")
    private String password;

    @Nullable
    @Expose
    @SerializedName("u_date_created")
    @ColumnInfo(name = "u_date_created")
    private String dateCreated;

    @Ignore
    public User(String userName) {
        this.uid = UUID.randomUUID().toString();
        this.userName = userName;
    }

    public User(String uid, String userName, String imgUrl, String dateCreated) {
        this.uid = uid;
        this.userName = userName;
        this.setImgUrl(imgUrl);
        this.setDateCreated(dateCreated);
    }

    @Ignore
    public User(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
        this.setImgUrl("");
        this.uid = "";
    }

    public String getUid() {
        return uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUsername(String username) {
        this.userName = username;
    }

    public boolean isEmpty() {
        return userName != null && TextUtils.isEmpty(uid) && TextUtils.isEmpty(userName);
    }

    @Nullable
    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(@Nullable String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    @Nullable
    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(@Nullable String dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(uid, user.uid) &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(imgUrl, user.imgUrl) &&
                Objects.equals(password, user.password) &&
                Objects.equals(dateCreated, user.dateCreated);
    }

    @Override
    public int hashCode() {

        return Objects.hash(uid, userName, imgUrl, password, dateCreated);
    }

    @Override
    public String toString() {
        return "User{" +
                "uid='" + uid + '\'' +
                ", userName='" + userName + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", password='" + password + '\'' +
                ", dateCreated='" + dateCreated + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
        dest.writeString(this.userName);
        dest.writeString(this.imgUrl);
        dest.writeString(this.password);
        dest.writeString(this.dateCreated);
    }

    protected User(Parcel in) {
        this.uid = in.readString();
        this.userName = in.readString();
        this.imgUrl = in.readString();
        this.password = in.readString();
        this.dateCreated = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
