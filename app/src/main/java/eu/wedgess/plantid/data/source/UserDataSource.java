package eu.wedgess.plantid.data.source;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.base.Optional;

import java.io.File;

import eu.wedgess.plantid.data.User;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Access point for accessing user data.
 * Implemented by: {@link eu.wedgess.plantid.data.source.remote.UsersRemoteDataSource} &
 * {@link eu.wedgess.plantid.data.source.local.UsersLocalDataSource}
 * <p>
 * Created by gar on 09/11/17.
 */
public interface UserDataSource {

    Maybe<User> getUserById(@NonNull String uid);

    /**
     * Inserts the user in the data source, or, if this is an existing user, it updates it.
     *
     * @param user the user to be inserted or updated.
     */
    Completable insertOrUpdateUser(@NonNull User user);

    Completable insertCurrentUserToPreferences(@NonNull User user);

    Single<User> logUserIn(@NonNull final String username, @NonNull final String password);

    Single<User> registerUser(@NonNull final User user, File image);

    Single<User> uploadProfileImage(@NonNull File profileImage);

    Single<Optional<User>> getCurrentUser();

    Completable deleteUser(@NonNull User user);

    Completable signUserOut(@NonNull User user);

    Completable deleteUserProfile(@NonNull String uid);

    Single<User> updateUserProfile(@NonNull User user, @Nullable File image);
}
