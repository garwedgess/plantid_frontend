package eu.wedgess.plantid.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.os.Parcel;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Comment object:
 * <p>
 * {@link ColumnInfo} is for Room database column name mapping
 * {@link SerializedName} is for GSON mapping
 * {@link Ignore} - ignore fields for Room db
 * <p>
 * Created by gar on 26/11/17.
 */

@Entity(tableName = "comments",
        foreignKeys = {
                @ForeignKey(entity = Thread.class,
                        parentColumns = "id",
                        childColumns = "c_thread_id",
                        onDelete = ForeignKey.CASCADE)},
        indices = {@Index(value = "c_thread_id")
        })
public class Comment extends Post {

    @Expose
    @NonNull
    @SerializedName("c_thread_id")
    @ColumnInfo(name = "c_thread_id")
    private String threadId;

    @Expose
    @SerializedName("c_is_answer")
    @ColumnInfo(name = "c_is_answer")
    private boolean isAnswer;

    @Expose
    @ColumnInfo(name = "c_dirty")
    private boolean dirty;

    @Ignore
    private User author;


    public Comment(String pid, String message, String authorId, String dateCreated, @NonNull String threadId, boolean isAnswer, boolean dirty) {
        super(pid, message, authorId, dateCreated);
        this.threadId = threadId;
        this.isAnswer = isAnswer;
        this.dirty = dirty;
    }

    @NonNull
    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(@NonNull String threadId) {
        this.threadId = threadId;
    }

    public boolean isAnswer() {
        return isAnswer;
    }

    public void setAnswer(boolean answer) {
        isAnswer = answer;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public User getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "threadId='" + threadId + '\'' +
                ", commentId=" + getId() +
                ", message=" + getMessage() +
                ", authorID=" + getAuthorId() +
                ", isAnswer=" + isAnswer +
                '}';
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;
        if (!super.equals(o)) return false;

        Comment comment = (Comment) o;

        if (isAnswer != comment.isAnswer) return false;
        if (dirty != comment.dirty) return false;
        if (!threadId.equals(comment.threadId)) return false;
        return author != null ? author.equals(comment.author) : comment.author == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + threadId.hashCode();
        result = 31 * result + (isAnswer ? 1 : 0);
        result = 31 * result + (dirty ? 1 : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        return result;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.threadId);
        dest.writeByte(this.isAnswer ? (byte) 1 : (byte) 0);
        dest.writeByte(this.dirty ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.author, flags);
    }

    protected Comment(Parcel in) {
        this.threadId = in.readString();
        this.isAnswer = in.readByte() != 0;
        this.dirty = in.readByte() != 0;
        this.author = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
}
