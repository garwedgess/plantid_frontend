package eu.wedgess.plantid.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Super class of {@link Thread} and {@link Comment} classes.
 * <p>
 * {@link ColumnInfo} is for Room database column name mapping
 * {@link SerializedName} is for GSON mapping
 * <p>
 * <p>
 * Created by gar on 26/11/17.
 */
public abstract class Post implements Parcelable {

    @Expose
    @PrimaryKey
    @NonNull
    @SerializedName("id")
    @ColumnInfo(name = "id")
    public String pid;

    @Expose
    @NonNull
    @SerializedName("message")
    @ColumnInfo(name = "message")
    private String message;

    @Expose
    @NonNull
    @SerializedName("author_id")
    @ColumnInfo(name = "author_id")
    private String authorId;

    @Expose
    @Nullable
    @SerializedName("date_created")
    @ColumnInfo(name = "date_created")
    private String dateCreated;

    @Ignore
    public Post() {

    }

    public Post(String pid, String message, String authorId, String dateCreated) {
        this.setId(pid);
        this.setMessage(message);
        this.setAuthorId(authorId);
        this.setDateCreated(dateCreated);
    }

    @NonNull
    public String getId() {
        return pid;
    }

    public void setId(String pid) {
        this.pid = pid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Post)) return false;

        Post post = (Post) o;

        if (!pid.equals(post.pid)) return false;
        if (!message.equals(post.message)) return false;
        if (!authorId.equals(post.authorId)) return false;
        return dateCreated != null ? dateCreated.equals(post.dateCreated) : post.dateCreated == null;
    }

    @Override
    public int hashCode() {
        int result = pid.hashCode();
        result = 31 * result + message.hashCode();
        result = 31 * result + authorId.hashCode();
        result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
        return result;
    }
}
