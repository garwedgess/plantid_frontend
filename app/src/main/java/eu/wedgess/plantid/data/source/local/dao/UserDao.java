package eu.wedgess.plantid.data.source.local.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import eu.wedgess.plantid.data.User;
import io.reactivex.Maybe;

/**
 * Databse access object for the user table {@link User}
 * <p>
 * Created by gar on 10/11/17.
 */

@Dao
public interface UserDao {

    @Delete
    void delete(User user);

    /**
     * Delete a user by id.
     *
     * @return the number of user deleted. This should always be 1.
     */
    @Query("DELETE FROM user WHERE u_id = :uid")
    int deleteUserById(String uid);
    
    /**
     * Select a user by id.
     *
     * @param uid the user id.
     * @return the user with uid.
     */
    @Query("SELECT * FROM user WHERE u_id = :uid")
    Maybe<User> getUserById(String uid);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(User user);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(User user);

}