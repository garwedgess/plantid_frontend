package eu.wedgess.plantid.data.source.local;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.List;

import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.data.source.PlantDataSource;
import eu.wedgess.plantid.data.source.local.dao.PlantDao;
import eu.wedgess.plantid.data.source.repositories.PlantsRepository;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Local data source for connecting to the plant table of the local database.
 * Used by: {@link PlantsRepository}
 * <p>
 * Created by gar on 10/11/17.
 */

@SuppressWarnings({"Guava", "Convert2MethodRef"})
public class PlantsLocalDataSource implements PlantDataSource {

    private static final String TAG = PlantsLocalDataSource.class.getSimpleName();

    private final PlantDao mPlantsDao;

    public PlantsLocalDataSource(@NonNull PlantDao plantDao) {
        mPlantsDao = checkNotNull(plantDao);
    }

    @Override
    public Observable<List<Plant>> getPlants() {
        return mPlantsDao.getAllPlants().toObservable();
    }


    @Override
    public Single<Optional<Plant>> getPlantById(@NonNull final String pid) {
        return mPlantsDao
                .getPlantById(pid)
                .map(plant -> Optional.fromNullable(plant));
    }

    @Override
    public Completable insertOrUpdatePlant(final Plant plant) {
        return Completable
                .fromAction(() -> mPlantsDao
                        .insertPlant(plant));
    }

    @Override
    public void refreshPlants() {
        // Not required because the {@link PlantsRepository} handles the logic of refreshing the
        // plants from all the available data sources.
    }
}
