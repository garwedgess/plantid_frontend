package eu.wedgess.plantid.data.source.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import eu.wedgess.plantid.data.History;
import eu.wedgess.plantid.data.source.HistoryDataSource;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The repository is responsible of handling History data operations.
 * Mainly used by {@link eu.wedgess.plantid.features.history.HistoryPresenter}
 * <p>
 * Created by gar on 10/11/17.
 */
public class HistoryRepository implements HistoryDataSource {

    private static final String TAG = HistoryRepository.class.getSimpleName();

    private final HistoryDataSource mHistoryLocalDataSource;
    private final HistoryDataSource mHistoryRemoteDataSource;

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    private Map<String, History> mCachedHistory;
    private boolean mCacheIsDirty = false;

    private static volatile HistoryRepository sSoleInstance;

    public static HistoryRepository getInstance(
            @NonNull HistoryDataSource historyLocalDataSource,
            @NonNull HistoryDataSource historyRemoteDataSource) {
        //Double check locking pattern
        if (sSoleInstance == null) { //Check for the first time

            synchronized (HistoryRepository.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (sSoleInstance == null)
                    sSoleInstance = new HistoryRepository(historyLocalDataSource, historyRemoteDataSource);
            }
        }

        return sSoleInstance;
    }

    private HistoryRepository(HistoryDataSource historyLocalDataSource, HistoryDataSource historyRemoteDataSource) {
        //Prevent form the reflection api.
        if (sSoleInstance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
        mHistoryLocalDataSource = checkNotNull(historyLocalDataSource);
        mHistoryRemoteDataSource = checkNotNull(historyRemoteDataSource);
    }

    @Nullable
    private History getHistoryWithId(@NonNull String id) {
        checkNotNull(id);
        if (mCachedHistory != null && !mCachedHistory.isEmpty()) {
            for (History history : mCachedHistory.values()) {
                if (history.getId().equals(id)) {
                    return history;
                }
            }
        }
        return null;
    }

    public void clearCache() {
        mCachedHistory = null;
    }

    private Observable<List<History>> getAndSaveRemoteHistory(@NonNull String uid) {
        return mHistoryRemoteDataSource
                .getHistory(uid)
                .flatMap(Observable::fromIterable)
                .doOnNext(history -> {
                    mHistoryLocalDataSource
                            .insertHistoryItem(history)
                            .subscribeOn(Schedulers.io())
                            .observeOn(Schedulers.io())
                            .subscribe(() -> Log.i(TAG, "Inserted history item locally"),
                                    throwable -> {
                                        Log.e(TAG, "Error inserting history item locally");
//                                        throw Exceptions.propagate(throwable);
                                    });
                })
                .toList()
                .toObservable();
    }

    private Observable<List<History>> getAndCacheLocalHistory(@NonNull String uid) {
        return mHistoryLocalDataSource
                .getHistory(uid)
                .flatMap(historyList -> Observable.fromIterable(historyList)
                        .doOnNext(history -> mCachedHistory.put(history.getId(), history))
                        .toList()
                        .toObservable());
    }


    @Override
    public Observable<List<History>> getHistory(@NonNull final String uid) {
        // Respond immediately with cache if available and not dirty
        if (mCachedHistory != null && !mCachedHistory.isEmpty() && !mCacheIsDirty) {
            Log.i(TAG, "[CACHE] Got history from cache");
            List<History> history = new ArrayList<>(mCachedHistory.values());
            return Observable.just(history);
        } else if (mCachedHistory == null) {
            mCachedHistory = new LinkedHashMap<>();
        }

        Observable<List<History>> remoteHistory = getAndSaveRemoteHistory(uid);
        Observable<List<History>> localHistory = getAndCacheLocalHistory(uid);

        if (mCacheIsDirty) {
            // If the cache is dirty we need to fetch new data from the network.
            return remoteHistory.onErrorResumeNext(localHistory);
        } else {
            // Query the network if available. If not, query local storage.
            return remoteHistory
                    .onErrorResumeNext(localHistory)
                    .publish(network ->
                            localHistory
                                    .takeUntil(network)
                                    .take(1)
                                    .filter(list -> !list.isEmpty())
                                    .switchIfEmpty(network));
        }
    }

    @Override
    public Single<History> getHistoryItemById(@NonNull final String uid, @NonNull final String hid) {
        checkNotNull(hid);
        History cachedHistoryItem = getHistoryWithId(hid);
        // Respond immediately with cache if available
        if (cachedHistoryItem != null) {
            return Single.just(cachedHistoryItem);
        }

        Single<History> localHistoryItem = mHistoryLocalDataSource.getHistoryItemById(uid, hid);
        Single<History> remoteHistoryItem = mHistoryRemoteDataSource.getHistoryItemById(uid, hid);

        return remoteHistoryItem
                .onErrorResumeNext(localHistoryItem);
    }

    @Override
    public Completable insertHistoryItem(@NonNull History historyItem) {
        cacheHistoryItem(historyItem);
        return mHistoryLocalDataSource
                .insertHistoryItem(historyItem);
    }

    @Override
    public Completable updateHistoryItem(@NonNull History historyItem) {
        cacheHistoryItem(historyItem);
        return mHistoryLocalDataSource
                .updateHistoryItem(historyItem)
                .andThen(mHistoryRemoteDataSource
                        .updateHistoryItem(historyItem));
    }

    @Override
    public Completable deleteHistoryItems(@NonNull List<History> historyItems) {
        deleteItemsFromCache(historyItems);
        return mHistoryLocalDataSource
                .deleteHistoryItems(historyItems)
                .andThen(mHistoryRemoteDataSource
                        .deleteHistoryItems(historyItems));
    }

    @Override
    public Completable deleteHistoryItem(@NonNull History history) {
        return mHistoryLocalDataSource.deleteHistoryItem(history)
                .andThen(Completable.fromAction(() -> deleteItemFromCache(history)));
    }

    private void deleteItemFromCache(History history) {
        History hist = getHistoryWithId(history.getId());
        if (hist != null && hist.getId().equals(history.getId())) {
            mCachedHistory.remove(hist.getId());
        }
    }

    private void deleteItemsFromCache(List<History> historyItems) {
        for (History item : historyItems) {
            History hist = getHistoryWithId(item.getId());
            if (hist != null && hist.getId().equals(item.getId())) {
                mCachedHistory.remove(hist.getId());
            }
        }
    }

    private void cacheHistoryItem(@NonNull History historyItem) {
        if (mCachedHistory == null) {
            mCachedHistory = new LinkedHashMap<>();
        }
        mCachedHistory.put(historyItem.getId(), historyItem);
    }

    @Override
    public void refreshHistoryItems() {
        mCacheIsDirty = true;
    }

    @Override
    public Single<List<History>> getDirtyHistory() {
        return mHistoryLocalDataSource.getDirtyHistory();
    }

    @Override
    public Completable updateDirtyHistory(@NonNull History history) {
        return mHistoryRemoteDataSource.updateHistoryItem(history)
                .andThen(mHistoryLocalDataSource
                        .updateHistoryItem(history)
                        .doOnComplete(() -> {
                            if (mCachedHistory != null) {
                                mCachedHistory.put(history.getId(), history);
                            }
                        }));
    }
}
