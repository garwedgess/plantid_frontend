package eu.wedgess.plantid.data.source;

import android.support.annotation.NonNull;

import com.google.common.base.Optional;

import java.util.List;

import eu.wedgess.plantid.data.Plant;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Access point for accessing plant data.
 * Implemented by both: {@link eu.wedgess.plantid.data.source.remote.PlantsRemoteDataSource} &
 * {@link eu.wedgess.plantid.data.source.local.PlantsLocalDataSource}
 * <p>
 * Created by gar on 09/11/17.
 */
public interface PlantDataSource {

    Observable<List<Plant>> getPlants();

    Single<Optional<Plant>> getPlantById(@NonNull String pid);

    /**
     * Inserts the plant in the data source, or, if this is an existing plant, it updates it.
     *
     * @param plant the plant to be inserted or updated.
     */
    Completable insertOrUpdatePlant(Plant plant);

    void refreshPlants();
}
