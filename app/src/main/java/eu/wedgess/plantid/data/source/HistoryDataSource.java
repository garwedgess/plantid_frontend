package eu.wedgess.plantid.data.source;

import android.support.annotation.NonNull;

import java.util.List;

import eu.wedgess.plantid.data.History;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Access point for accessing history data.
 * Implemented by both: {@link eu.wedgess.plantid.data.source.remote.HistoryRemoteDataSource} &
 * {@link eu.wedgess.plantid.data.source.local.HistoryLocalDataSource}
 * <p>
 * Created by gar on 09/11/17.
 */
public interface HistoryDataSource {

    Observable<List<History>> getHistory(@NonNull String uid);

    Single<History> getHistoryItemById(@NonNull String uid, @NonNull String hid);

    /**
     * Inserts the history item in the data source, or, if this is an existing history, it updates it.
     *
     * @param historyItem the {@link History} item to be inserted or updated.
     */
    Completable insertHistoryItem(@NonNull History historyItem);

    Completable updateHistoryItem(@NonNull History historyItem);

    Completable updateDirtyHistory(@NonNull History historyItem);

    Completable deleteHistoryItems(@NonNull List<History> historyItems);

    Completable deleteHistoryItem(@NonNull History history);

    Single<List<History>> getDirtyHistory();

    void refreshHistoryItems();
}
