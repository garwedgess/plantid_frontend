package eu.wedgess.plantid.data.source.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import eu.wedgess.plantid.data.Comment;
import eu.wedgess.plantid.data.source.CommentsDataSource;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The repository is responsible of handling comments data operations {@link Comment}.
 * Mainly used in {@link eu.wedgess.plantid.features.threaddetails.ThreadDetailPresenter}
 * <p>
 * <p>
 * Created by gar on 1/12/17.
 */
public class CommentsRepository implements CommentsDataSource {

    private static final String TAG = CommentsRepository.class.getSimpleName();

    private final CommentsDataSource mCommentsLocalDataSource;
    private final CommentsDataSource mCommentsRemoteDataSource;

    // cached comments used for in memory cache
    private Map<String, Comment> mCachedComments;
    private boolean mCacheIsDirty = false;

    private static volatile CommentsRepository sSoleInstance;

    public static CommentsRepository getInstance(@NonNull CommentsDataSource commentsLocalDataSource,
                                                 @NonNull CommentsDataSource commentsRemoteDataSource) {
        //Double check locking pattern
        if (sSoleInstance == null) { //Check for the first time

            synchronized (CommentsRepository.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (sSoleInstance == null)
                    sSoleInstance = new CommentsRepository(commentsLocalDataSource, commentsRemoteDataSource);
            }
        }

        return sSoleInstance;
    }


    private CommentsRepository(@NonNull CommentsDataSource commentsLocalDataSource,
                               @NonNull CommentsDataSource commentsRemoteDataSource) {
        //Prevent form the reflection api.
        if (sSoleInstance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
        mCommentsLocalDataSource = checkNotNull(commentsLocalDataSource);
        mCommentsRemoteDataSource = checkNotNull(commentsRemoteDataSource);
    }

    private Observable<List<Comment>> getAndSaveRemoteCommentsByThreadId(String threadId) {
        return mCommentsRemoteDataSource
                .getCommentsByThreadId(threadId)
                .flatMap(comments -> Observable.fromIterable(comments)
                        .doOnNext(comment -> {
                            mCommentsLocalDataSource
                                    .insertOrUpdateComment(comment)
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(() -> {
                                        if (mCachedComments == null) {
                                            mCachedComments = new LinkedHashMap<>();
                                        }
                                        mCachedComments.put(comment.getId(), comment);
                                    });
                        })
                        .toList()
                        .toObservable())
                .doOnComplete(() -> mCacheIsDirty = false);
    }

    /*
     * Get threads from the local DB and cache them when received.
     */
    private Observable<List<Comment>> getAndCacheLocalCommentsByThreadId(@NonNull String threadId) {
        if (mCachedComments == null) {
            mCachedComments = new LinkedHashMap<>();
        }
        return mCommentsLocalDataSource
                .getCommentsByThreadId(threadId)
                .flatMap(commentList -> Observable.fromIterable(commentList)
                        .doOnNext(comment -> {
                            mCachedComments.put(comment.getId(), comment);
                        })
                        .toList()
                        .toObservable());
    }

    private void refreshCache(List<Comment> comments) {
        if (mCachedComments == null) {
            mCachedComments = new LinkedHashMap<>();
        }
        mCachedComments.clear();
        for (Comment comment : comments) {
            mCachedComments.put(comment.getId(), comment);
        }
        mCacheIsDirty = false;
    }

    /**
     * Get comment by id from cache if its found.
     *
     * @param id
     * @return
     */
    @Nullable
    private Comment getCommentWithId(@NonNull String id) {
        checkNotNull(id);
        if (mCachedComments != null && !mCachedComments.isEmpty()) {
            for (Comment comment : mCachedComments.values()) {
                if (comment.getId().equals(id)) {
                    return comment;
                }
            }
        }
        return null;
    }


    @Override
    public Single<Comment> getCommentById(@NonNull final String tid, @NonNull final String cid) {
        checkNotNull(cid, "Can't get comment by id when id is NULL");
        checkNotNull(tid, "Can't get comment by id when thread id is NULL");

        Comment cachedComment = getCommentWithId(cid);
        if (cachedComment != null) {
            return Single.just(cachedComment);
        }

        Single<Comment> localComment = mCommentsLocalDataSource.getCommentById(tid, cid);
        Single<Comment> remoteComment = mCommentsRemoteDataSource.getCommentById(tid, cid);

        return remoteComment.onErrorResumeNext(localComment);
    }


    private List<Comment> getCachedCommentsByThreadId(@NonNull final String tid) {
        checkNotNull(tid);
        List<Comment> threadsComments = new ArrayList<>();
        for (Comment comment : mCachedComments.values()) {
            if (comment.getThreadId().equals(tid)) {
                threadsComments.add(comment);
            }
        }
        return threadsComments;
    }

    @Override
    public Observable<List<Comment>> getCommentsByThreadId(@NonNull final String threadId) {
        checkNotNull(threadId);

        // get comments from cache
        if (mCachedComments != null && !mCacheIsDirty) {
            List<Comment> cachedComments = getCachedCommentsByThreadId(threadId);
            if (!cachedComments.isEmpty()) {
                Log.i(TAG, "Got comments from cache: " + cachedComments.size());
                return Observable.just(cachedComments);
            }
        }

        Observable<List<Comment>> remoteComments = getAndSaveRemoteCommentsByThreadId(threadId);
        Observable<List<Comment>> localComments = getAndCacheLocalCommentsByThreadId(threadId);

        if (mCacheIsDirty) {
            return remoteComments.onErrorResumeNext(localComments);
        } else {
            return remoteComments
                    .onErrorResumeNext(localComments)
                    .publish(network ->
                            Observable
                                    .concat(network, localComments
                                            .takeUntil(network)));
        }
    }

    @Override
    public Completable insertOrUpdateComment(@NonNull Comment comment) {
        return mCommentsLocalDataSource
                .insertOrUpdateComment(comment)
                .doOnComplete(() -> {
                    if (mCachedComments != null) {
                        mCachedComments.put(comment.getId(), comment);
                    }
                })
                .andThen(mCommentsRemoteDataSource
                        .insertOrUpdateComment(comment));
    }

    @Override
    public Completable updateComment(@NonNull Comment comment) {
        return mCommentsLocalDataSource
                .updateComment(comment
                ).doOnComplete(() -> {
                    if (mCachedComments != null) {
                        mCachedComments.put(comment.getId(), comment);
                    }
                }).andThen(mCommentsRemoteDataSource.updateComment(comment)
                        .doOnError(throwable -> {
                            comment.setDirty(true);
                            mCommentsLocalDataSource
                                    .updateComment(comment)
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(() -> {
                                        Log.e(TAG, "updated comment as dirty and saved locally because could not update in remote DB");
                                    });
                        }));
    }

    @Override
    public Completable updateDirtyComment(@NonNull Comment comment) {
        return mCommentsRemoteDataSource.insertOrUpdateComment(comment)
                .andThen(mCommentsLocalDataSource
                        .updateComment(comment)
                        .doOnComplete(() -> {
                            if (mCachedComments != null) {
                                mCachedComments.put(comment.getId(), comment);
                            }
                        }));
    }

    @Override
    public Completable deleteComment(@NonNull Comment comment) {
        return mCommentsLocalDataSource.deleteComment(comment).doOnComplete(() -> {
            if (mCachedComments != null) {
                // must itterate through map when modifying, no for loops
                for (Iterator<Map.Entry<String, Comment>> i = mCachedComments.entrySet().iterator(); i.hasNext(); ) {
                    Map.Entry<String, Comment> entry = i.next();
                    if (entry.getValue().getId().equals(comment.getId())) {
                        i.remove();
                    }
                }
            }
        }).andThen(mCommentsRemoteDataSource
                .deleteComment(comment));
    }

    @Override
    public void refreshComments() {
        mCacheIsDirty = true;
    }

    @Override
    public Single<List<Comment>> getDirtyComments() {
        return mCommentsLocalDataSource.getDirtyComments();
    }
}
