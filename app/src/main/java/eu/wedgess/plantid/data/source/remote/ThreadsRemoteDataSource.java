package eu.wedgess.plantid.data.source.remote;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;
import java.util.concurrent.TimeUnit;

import eu.wedgess.plantid.data.Thread;
import eu.wedgess.plantid.data.source.ThreadsDataSource;
import eu.wedgess.plantid.network.RetrofitInstance;
import eu.wedgess.plantid.network.api.requests.ThreadRequests;
import eu.wedgess.plantid.network.api.responses.ThreadResponse;
import eu.wedgess.plantid.network.api.responses.ThreadsListResponse;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Singleton Retrofit class to connect to the API endpoints for threads.{@link Thread}
 * Endpoints are found at: {@link ThreadRequests}
 * <p>
 * Created by gar on 11/11/17.
 */

public class ThreadsRemoteDataSource implements ThreadsDataSource {

    private static final String TAG = ThreadsRemoteDataSource.class.getSimpleName();
    private static final int NETWORK_TIMEOUT_SEC = 6;

    private final ThreadRequests mThreadRequestService;

    public ThreadsRemoteDataSource() {
        mThreadRequestService = RetrofitInstance.getRetrofitInstance().create(ThreadRequests.class);
    }

    /**
     * Gets all the threads from servers DB.
     *
     * @return
     */
    @Override
    public Observable<List<Thread>> getThreads() {
        return mThreadRequestService
                .getAllThreads()
                .toObservable()
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS)
                .map(ThreadsListResponse::getThreads)
                .doOnNext(threadList -> {
                    Log.i(TAG, "Got threads from remote: " + threadList.size());
                });
    }

    @Override
    public Single<Thread> getThreadById(@NonNull final String tid) {
        return Single
                .fromObservable(mThreadRequestService.getThreadById(tid)
                        .map(ThreadResponse::getThread)
                        .toObservable())
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public Observable<List<Thread>> getThreadsByAuthorId(@NonNull String authorId) {
        return mThreadRequestService
                .getThreadsByAuthorId(authorId)
                .map(ThreadsListResponse::getThreads)
                .toObservable()
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public Completable insertThread(@NonNull Thread thread) {
        return mThreadRequestService
                .createThread(thread)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public Completable updateThread(@NonNull Thread thread) {
        return mThreadRequestService
                .updateThread(thread.getId(), thread)
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public Completable deleteThread(@NonNull Thread thread) {
        return mThreadRequestService
                .deleteThread(thread.getId())
                .timeout(NETWORK_TIMEOUT_SEC, TimeUnit.SECONDS);
    }

    @Override
    public void refreshThreads() {
        // IGNORE - Handled by main data source
    }

    @Override
    public Single<List<Thread>> getDirtyThreads() {
        throw new RuntimeException("Threads are not dirty on remote. Only Locally!");
    }

    @Override
    public Completable updateDirtyThread(@NonNull Thread thread) {
        return insertThread(thread);
    }
}
