/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import eu.wedgess.plantid.data.AppDatabase;
import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.data.source.local.PlantsLocalDataSource;

/**
 * Integration tests for the {@link PlantsLocalDataSource} implementation with Room.
 * Created by gar on 07/03/18.
 */
public class LocalPlantsDataSourceTest {

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    // Plant 1
    private static final Plant PLANT = new Plant("7fe7cbbb09444443b46132482a448db1", "", "Daphne bholua 'Jacqueline Postill'",
            "Daphne", "Shrub", "", "", "", "Pink", "Green",
            "Scented flowers", "Description", "Full sun", "", "Hardy", "",
            "Drained", "", "", "", "", "", "", "", "", "", "");
    // Plant 2
    private static final Plant PLANT2 = new Plant("4et1vrtyh1356789s1234fgtra448ed2", "", "Japanese Aralia",
            "Fatsia Japonica", "Shrub", "", "", "", "Pink", "Green",
            "Scented flowers", "Description", "Full sun", "", "Hardy", "",
            "Drained", "", "", "", "", "", "", "", "", "", "");


    private PlantsLocalDataSource mDataSource;

    @Before
    public void initDb() throws Exception {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        AppDatabase mDatabase = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                AppDatabase.class)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();
        mDataSource = new PlantsLocalDataSource(mDatabase.plantDao());
    }

    @SuppressWarnings("Guava")
    @Test
    public void insertAndGetPlant() {
        // When inserting a new plant in the data source, need to block and wait til plant is inserted
        mDataSource.insertOrUpdatePlant(PLANT).blockingAwait();

        mDataSource
                .getPlantById(PLANT.getId())
                .test()
                // assertValue asserts that there was only one emission of the plant by its id
                .assertValue(plant -> {
                    // The emitted plant is the expected one
                    return plant != null && plant.isPresent() && plant.get().getId().equals(PLANT.getId()) &&
                            plant.get().getCommonName().equals(PLANT.getCommonName());
                });
    }

    @Test
    public void insertAndGetPlants() {
        // Add 2 plant items
        mDataSource.insertOrUpdatePlant(PLANT).blockingAwait();
        mDataSource.insertOrUpdatePlant(PLANT2).blockingAwait();

        // test getting the plants and ensuring the list is not empty and contains the 2 items
        mDataSource
                .getPlants()
                .test()
                // assertValue asserts that there was only one emission of the plants
                .assertValue(plants -> {
                    // The emitted plants are not empty and has 2 items
                    return !plants.isEmpty() && plants.size() == 2;
                });
    }
}
