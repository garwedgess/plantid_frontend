/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid.utils;

import android.annotation.TargetApi;
import android.os.Build;

import com.google.gson.Gson;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

/**
 * Parsing rules and helper to load JSOn from resources, links to {@link JsonFileResource} annotation.
 */
public class JsonParsingRule implements TestRule {
    private final Gson mGson;
    private Object mValue;

    public JsonParsingRule(Gson gson) {
        mGson = gson;
    }

    @SuppressWarnings("unchecked")
    public <T> T getValue() {
        return (T) mValue;
    }

    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void evaluate() throws Throwable {
                JsonFileResource jsonFileResource = description.getAnnotation(JsonFileResource.class);
                if (jsonFileResource != null) {
                    Class<?> clazz = jsonFileResource.clazz();
                    String resourceName = jsonFileResource.fileName();
                    Class<?> testClass = description.getTestClass();
                    InputStream in = testClass.getResourceAsStream(resourceName);

                    assert in != null : "Failed to load resource: " + resourceName + " from " + testClass;
                    try (Reader reader = new BufferedReader(new InputStreamReader(in))) {
                        mValue = mGson.fromJson(reader, clazz);
                    }
                }
                base.evaluate();
            }
        };
    }
}
