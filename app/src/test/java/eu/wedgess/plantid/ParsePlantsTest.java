/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.wedgess.plantid.data.Plant;
import eu.wedgess.plantid.utils.JsonFileResource;
import eu.wedgess.plantid.utils.JsonParsingRule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

/**
 * Test parsing plant JSOn to plant object {@link Plant}
 * Created by gar on 07/03/18.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(TextUtils.class)
public class ParsePlantsTest {

    public static final Gson GSON;

    static {
        GSON = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Before
    public void setup() {
        PowerMockito.mockStatic(TextUtils.class);
        PowerMockito.when(TextUtils.isEmpty(any(CharSequence.class))).thenAnswer(invocation -> {
            CharSequence a = (CharSequence) invocation.getArguments()[0];
            return !(a != null && a.length() > 0);
        });
    }

    @Rule
    public JsonParsingRule jsonParsingRule = new JsonParsingRule(GSON);

    @Test
    @JsonFileResource(fileName = "plants.json", clazz = Plant[].class)
    public void testGetPlants() throws Exception {
        Plant[] plants = jsonParsingRule.getValue();
        // should have parsed 5 plants
        assertThat(plants).hasSize(5);
        // ensure that the first item is a "Daphne"
        assertThat(plants[0].getCommonName()).isEqualTo("Daphne");
    }
}