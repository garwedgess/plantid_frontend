/*
 * Copyright (c) 2018 Gareth Williams.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package eu.wedgess.plantid;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import eu.wedgess.plantid.utils.DateUtility;

import static org.junit.Assert.assertEquals;

/**
 * Testing teh DateUtility class --
 * NOTE: Cannot test relative date as this will fail depending on the day/month the test is run.
 * <p>
 * Created by gar on 17/02/18.
 */
public class DateUtilityTest {

    private Date getDBDate(String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        return sdf.parse(dateStr);
    }

    @Test
    public void testCanConvertServerTimeToNormal() {
        String date = "2018-02-14T00:13:52+00:00";
        String expectedDate = "Wed Feb 14 00:13:52 GMT 2018";
        Date normalDate = null;
        try {
            normalDate = DateUtility.serverDateToNormalDateObj(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (normalDate != null) {
            assertEquals(expectedDate, normalDate.toString());
        }
    }

    @Test
    public void testCanConvertDateToHistoryDate() throws ParseException {
        String date = "2018-02-14T00:13:52+00:00";
        String expectedDate = "14 February";
        assertEquals(expectedDate, DateUtility.dateToHistoryDateString(date));
    }

    @Test
    public void testCanCanConvertDateToProfileDate() throws ParseException {
        String date = "2018-02-14T00:13:52+00:00";
        String expectedDate = "14 February 2018";
        assertEquals(expectedDate, DateUtility.dateToProfileDateString(date));
    }
}